import React from "react";

import { sample } from "../../utils";
import { WORDS } from "../../data";
import { NUM_OF_GUESSES_ALLOWED } from "../../constants";
import GuessInput from "../GuessInput/GuessInput";
import GuessResults from "../GuessResults/GuessResults";
import { checkGuess } from "../../game-helpers";
import Banner from "../Banner/Banner";

// Pick a random word on every pageload.
const answer = sample(WORDS);
// To make debugging easier, we'll log the solution in the console.
console.info({ answer });

function Game() {
  const [guesses, setGuesses] = React.useState([]);
  const [bannerType, setBannerType] = React.useState(null);
  return (
    <>
      <GuessResults guesses={guesses} />
      <GuessInput
        disabled={bannerType !== null}
        handleGuess={(guess) => {
          const newGuesses = [...guesses, checkGuess(guess, answer)];
          if (guess === answer) {
            setBannerType("happy");
          }
          if (newGuesses.length === NUM_OF_GUESSES_ALLOWED) {
            setGuesses(newGuesses);
            if (guess !== answer) {
              setBannerType("sad");
            }
          } else if (newGuesses.length < NUM_OF_GUESSES_ALLOWED) {
            setGuesses(newGuesses);
          }
        }}
      />
      {bannerType && (
        <Banner
          type={bannerType}
          answer={answer}
          num_of_guesses={guesses.length}
        />
      )}
    </>
  );
}

export default Game;
