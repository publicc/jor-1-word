import React from "react";
import { range } from "../../utils";

function Guess({ guess }) {
  return (
    <>
      {range(5).map((idx) => (
        <span key={idx} className={`cell ${guess && guess[idx].status}`}>
          {guess && guess[idx].letter}
        </span>
      ))}
    </>
  );
}

export default Guess;
