import React from "react";

function Banner({ type, num_of_guesses, answer }) {
  const typeMsg = {
    happy: (
      <p>
        <strong>Congratulations!</strong> Got it in
        <strong> {num_of_guesses} guesses</strong>
      </p>
    ),
    sad: (
      <p>
        Sorry, the correct answer is <strong>{answer}</strong>.
      </p>
    ),
  };
  return <div className={`${type} banner`}>{typeMsg[type]}</div>;
}

export default Banner;
