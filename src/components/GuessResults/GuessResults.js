import React from "react";
import { range } from "../../utils";
import Guess from "../Guess/Guess";
import { NUM_OF_GUESSES_ALLOWED } from "../../constants";

function GuessResults({ guesses }) {
  const rows = range(NUM_OF_GUESSES_ALLOWED).map((idx) => {
    return (
      <p key={idx} className="guess">
        <Guess guess={guesses[idx]} />
      </p>
    );
  });

  return <div className="guess-results">{rows}</div>;
}

export default GuessResults;
