import React from "react";

function GuessInput({ handleGuess, disabled }) {
  const [guess, setGuess] = React.useState("");
  const handleSubmit = (event) => {
    event.preventDefault();
    if (guess.length !== 5) {
      alert("Input should be 5 characters length");
      return;
    }

    handleGuess(guess);
    setGuess("");
  };
  return (
    <form className="guess-input-wrapper" onSubmit={handleSubmit}>
      <label htmlFor="guess-input">Enter guess:</label>
      <input
        disabled={disabled}
        type="text"
        id="guess-input"
        value={guess}
        onChange={(event) => {
          const inp = event.target.value.toUpperCase();
          if (inp.length > 5) return;
          setGuess(inp);
        }}
      />
    </form>
  );
}

export default GuessInput;
